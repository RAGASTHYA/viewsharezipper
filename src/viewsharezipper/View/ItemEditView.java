/*
 * All contents of this project belongs to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.View;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import properties_manager.PropertiesManager;
import viewsharezipper.LanguagePropertyType;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_DIT_LOCATION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_EDIT_CATEGORY;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_EDIT_DATEISO;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_EDIT_TEXT;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_EDIT_TITLE;
import viewsharezipper.Model.Item;
import static viewsharezipper.StartupConstants.CSS_CLASS_COMPONENT_STYLE;
import static viewsharezipper.StartupConstants.CSS_CLASS_ITEM_EDIT_VIEW;
import static viewsharezipper.StartupConstants.CSS_CLASS_TITLE_COMPONENT_STYLE;
import static viewsharezipper.StartupConstants.DEFAULT_IMAGE;
import static viewsharezipper.StartupConstants.DEFAULT_IMAGE_WIDTH;
import static viewsharezipper.StartupConstants.NO_VIDEO_IMAGE;
import static viewsharezipper.StartupConstants.PATH_IMAGES;
import static viewsharezipper.StartupConstants.SLASH;

/**
 * This UI component has the controls for editing a single item in a collection,
 * including controls for editing the title, selected image, video, text,
 * location and category.
 *
 * @author Rahul S Agasthya
 */
public class ItemEditView extends VBox {

    Item item;

    HBox titleBar;
    Label title;
    Label location;

    HBox itemContents;
    VBox mediaBar;
    Image image;
    ImageView imageView;
    Media video;
    MediaView videoView;
    MediaPlayer videoPlayer;

    VBox textBar;
    Label text;
    Label dateISO;
    Label category;

    PropertiesManager props = PropertiesManager.getPropertiesManager();

    /**
     * This is the constructor for building the GUI for the item object passed.
     *
     * @param initItem The object for which the GUI is built.
     */
    public ItemEditView(Item initItem) {
        this.getStyleClass().add(CSS_CLASS_ITEM_EDIT_VIEW);

        item = initItem;

        titleBar = new HBox();
        title = initChildLabel(titleBar, item.getTitle(), TOOLTIP_EDIT_TITLE, CSS_CLASS_TITLE_COMPONENT_STYLE);

        itemContents = new HBox();
        mediaBar = new VBox();
        imageView = new ImageView();
        videoView = new MediaView();
        updateMediaComponents();
        itemContents.getChildren().add(mediaBar);

        textBar = new VBox();
        location = initChildLabel(textBar, item.getLocation(), TOOLTIP_DIT_LOCATION, CSS_CLASS_COMPONENT_STYLE);
        dateISO = initChildLabel(textBar, item.getDateISO(), TOOLTIP_EDIT_DATEISO, CSS_CLASS_COMPONENT_STYLE);
        category = initChildLabel(textBar, item.getCategory(), TOOLTIP_EDIT_CATEGORY, CSS_CLASS_COMPONENT_STYLE);
        text = initChildLabel(textBar, item.getText(), TOOLTIP_EDIT_TEXT, CSS_CLASS_COMPONENT_STYLE);
        itemContents.getChildren().add(textBar);

        this.getChildren().add(titleBar);
        this.getChildren().add(itemContents);

        //initItemListeners();
    }

    /**
     * This method initializes a label, with the content, tooltip and CSS Style,
     * and places it in the pane.
     *
     * @param box The pane in which the label is placed.
     * @param contents The contents of the label.
     * @param tooltip Tooltip of the label.
     * @param cssClass Style sheet of the label.
     * @return label, which is initialized with the above parameters.
     */
    public Label initChildLabel(Pane box,
            String contents,
            LanguagePropertyType tooltip,
            String cssClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Label label = new Label(contents);
        label.getStyleClass().add(cssClass);
        Tooltip labelTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        label.setTooltip(labelTooltip);
        box.getChildren().add(label);
        return label;
    }

    /**
     * This method is used to update the media components like image and video.
     * It also adds the views to the pane.
     */
    public void updateMediaComponents() {
        if (item.getImageURL().equals(PATH_IMAGES + DEFAULT_IMAGE)) {
            File file = new File(PATH_IMAGES + DEFAULT_IMAGE);
            try {
                URL fileURL = file.toURI().toURL();
                image = new Image(fileURL.toExternalForm());
                imageView = new ImageView();
                imageView.setImage(image);
            } catch (MalformedURLException ex) {
                Logger.getLogger(ItemEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            image = new Image(item.getImageURL());
            imageView.setImage(image);
        }

        double scaledWidth = DEFAULT_IMAGE_WIDTH;
        double perc = scaledWidth / image.getWidth();
        double scaledHeight = image.getHeight() * perc;
        imageView.setFitWidth(scaledWidth);
        imageView.setFitHeight(scaledHeight);
        mediaBar.getChildren().add(imageView);

        if (item.containsVideo()) {
            if (mediaBar.getChildren().size() > 1) {
                mediaBar.getChildren().remove(mediaBar.getChildren().size() - 1);
            }
            video = new Media(item.getVideoURL());
            videoPlayer = new MediaPlayer(video);
            videoView = new MediaView(videoPlayer);
            mediaBar.getChildren().add(videoView);
        } else {
            File file = new File(PATH_IMAGES + NO_VIDEO_IMAGE);
            try {
                URL fileURL = file.toURI().toURL();
                Image videoImage = new Image(fileURL.toExternalForm());
                ImageView videoImageView = new ImageView();
                videoImageView.setImage(videoImage);

                mediaBar.getChildren().add(videoImageView);
            } catch (MalformedURLException ex) {
                Logger.getLogger(ItemEditView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @todo This method should define the different action listeners for each
     * of the components.
     *
     * public void initItemListeners() {}
     */
}

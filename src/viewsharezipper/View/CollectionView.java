/*
 * All contents of this project belongs to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.View;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import viewsharezipper.Controller.CollectionEditController;
import viewsharezipper.Controller.FileController;
import viewsharezipper.File.CollectionFileManager;
import viewsharezipper.LanguagePropertyType;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_ADD_ITEM;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_CHANGE_COLOR;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_CHANGE_FONT;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_CHANGE_LAYOUT;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_EXIT_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_EXPORT_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_HELP;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_LOAD_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_MOVE_ITEM_DOWN;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_MOVE_ITEM_UP;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_NEW_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_REARRANGE_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_REMOVE_ITEM;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_SAVEAS_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_SAVE_COLLECTION;
import static viewsharezipper.LanguagePropertyType.TOOLTIP_VIEW_COLLECTION;
import viewsharezipper.Model.Collection;
import viewsharezipper.Model.Item;
import static viewsharezipper.StartupConstants.CSS_CLASS_BUTTON;
import static viewsharezipper.StartupConstants.CSS_CLASS_FILE_TOOLBAR_HBOX;
import static viewsharezipper.StartupConstants.CSS_CLASS_ITEM_TOOLBAR_VBOX;
import static viewsharezipper.StartupConstants.CSS_CLASS_NAV_PANE_ITEM;
import static viewsharezipper.StartupConstants.CSS_CLASS_SELECTED_ITEM;
import static viewsharezipper.StartupConstants.CSS_CLASS_STYLE_TOOLBAR_HBOX;
import static viewsharezipper.StartupConstants.ICON_ADD_ITEM;
import static viewsharezipper.StartupConstants.ICON_CHANGE_COLOR;
import static viewsharezipper.StartupConstants.ICON_CHANGE_FONT;
import static viewsharezipper.StartupConstants.ICON_CHANGE_LAYOUT;
import static viewsharezipper.StartupConstants.ICON_EXIT_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_EXPORT_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_HELP;
import static viewsharezipper.StartupConstants.ICON_LOAD_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_MOVE_ITEM_DOWN;
import static viewsharezipper.StartupConstants.ICON_MOVE_ITEM_UP;
import static viewsharezipper.StartupConstants.ICON_NEW_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_REARRANGE_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_REMOVE_ITEM;
import static viewsharezipper.StartupConstants.ICON_SAVEAS_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_SAVE_COLLECTION;
import static viewsharezipper.StartupConstants.ICON_VIEW_COLLECTION;
import static viewsharezipper.StartupConstants.PATH_ICONS;
import static viewsharezipper.StartupConstants.SLASH;
import static viewsharezipper.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author Rahul S Agasthya
 */
public class CollectionView {

    Stage primaryStage;
    Scene primaryScene;

    BorderPane mainPane;

    HBox titleBar;
    HBox fileToolbarPane;
    Button newFileButton;
    Button loadFileButton;
    Button saveFileButton;
    Button saveAsFileButton;
    Button exportButton;
    Button viewButton;
    Button compareButton;
    Button rearrangeButton;
    Button helpButton;
    Button exitButton;

    HBox styleToolbarPane;
    Button editFontButton;
    Button editColorButton;
    Button editLayoutButton;

    HBox workspace;
    VBox itemControlBar;
    Button addItemButton;
    Button removeItemButton;
    Button moveUpItemButton;
    Button moveDownItemButton;

    VBox navPane;
    ScrollPane navScrollPane;

    Collection collection;

    FileController fileController;
    CollectionFileManager fileManager;
    CollectionEditController editController;

    /**
     *
     *
     * This should have a parameter of CollectionFileManager type.
     */
    public CollectionView(CollectionFileManager initFileManager) {
        fileManager = initFileManager;
        collection = new Collection(this);
        //initialize the Error Handlers here.
    }

    public Collection getCollection() {
        return collection;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    //Write a getter for the error handler.
    public void startUI(Stage initPrimaryStage, String windowTitle) {
        titleBar = new HBox();
        initFileToolbar();
        initStyleToolbar();
        initWorkspace();
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
        initEventHandlers();
    }

    private void initWorkspace() {
        workspace = new HBox();

        itemControlBar = new VBox();
        itemControlBar.getStyleClass().add(CSS_CLASS_ITEM_TOOLBAR_VBOX);

        addItemButton = initChildButton(itemControlBar, ICON_ADD_ITEM, TOOLTIP_ADD_ITEM, CSS_CLASS_BUTTON, false);
        removeItemButton = initChildButton(itemControlBar, ICON_REMOVE_ITEM, TOOLTIP_REMOVE_ITEM, CSS_CLASS_BUTTON, true);
        moveUpItemButton = initChildButton(itemControlBar, ICON_MOVE_ITEM_UP, TOOLTIP_MOVE_ITEM_UP, CSS_CLASS_BUTTON, true);
        moveDownItemButton = initChildButton(itemControlBar, ICON_MOVE_ITEM_DOWN, TOOLTIP_MOVE_ITEM_DOWN, CSS_CLASS_BUTTON, true);
        rearrangeButton = initChildButton(itemControlBar, ICON_REARRANGE_COLLECTION, TOOLTIP_REARRANGE_COLLECTION, CSS_CLASS_BUTTON, true);

        navPane = new VBox();
        navScrollPane = new ScrollPane(navPane);

        workspace.getChildren().add(itemControlBar);
        workspace.getChildren().add(navScrollPane);
    }

    /**
     * @todo Define a method called initEventHandlers which handle all action
     * events.
     */
    public void initEventHandlers() {
        fileController = new FileController(this, fileManager);

        newFileButton.setOnAction(e -> {
            fileController.handleNewCollectionRequest();
        });
        loadFileButton.setOnAction(e -> {
            fileController.handleLoadCollectionRequest();
        });
        saveFileButton.setOnAction(e -> {
            fileController.handleSaveCollectionRequest();
        });

        editController = new CollectionEditController(this);
        addItemButton.setOnAction(e -> {
            editController.processAddItemRequest();
        });
        removeItemButton.setOnAction(e -> {
            editController.processRemoveItemRequest();
        });
        moveUpItemButton.setOnAction(e -> {
            editController.processMoveItemUpRequest();
        });
        moveDownItemButton.setOnAction(e -> {
            editController.processMoveItemDownRequest();
        });
    }

    public void initFileToolbar() {
        fileToolbarPane = new HBox();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_FILE_TOOLBAR_HBOX);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        newFileButton = initChildButton(fileToolbarPane, ICON_NEW_COLLECTION, TOOLTIP_NEW_COLLECTION, CSS_CLASS_BUTTON, false);
        loadFileButton = initChildButton(fileToolbarPane, ICON_LOAD_COLLECTION, TOOLTIP_LOAD_COLLECTION, CSS_CLASS_BUTTON, false);
        saveFileButton = initChildButton(fileToolbarPane, ICON_SAVE_COLLECTION, TOOLTIP_SAVE_COLLECTION, CSS_CLASS_BUTTON, true);
        saveAsFileButton = initChildButton(fileToolbarPane, ICON_SAVEAS_COLLECTION, TOOLTIP_SAVEAS_COLLECTION, CSS_CLASS_BUTTON, true);
        helpButton = initChildButton(fileToolbarPane, ICON_HELP, TOOLTIP_HELP, CSS_CLASS_BUTTON, false);
        viewButton = initChildButton(fileToolbarPane, ICON_VIEW_COLLECTION, TOOLTIP_VIEW_COLLECTION, CSS_CLASS_BUTTON, true);
        exportButton = initChildButton(fileToolbarPane, ICON_EXPORT_COLLECTION, TOOLTIP_EXPORT_COLLECTION, CSS_CLASS_BUTTON, true);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT_COLLECTION, TOOLTIP_EXIT_COLLECTION, CSS_CLASS_BUTTON, false);

        titleBar.getChildren().add(fileToolbarPane);
    }

    public void initStyleToolbar() {
        styleToolbarPane = new HBox();
        styleToolbarPane.getStyleClass().add(CSS_CLASS_STYLE_TOOLBAR_HBOX);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        editFontButton = initChildButton(styleToolbarPane, ICON_CHANGE_FONT, TOOLTIP_CHANGE_FONT, CSS_CLASS_BUTTON, false);
        editColorButton = initChildButton(styleToolbarPane, ICON_CHANGE_COLOR, TOOLTIP_CHANGE_COLOR, CSS_CLASS_BUTTON, false);
        editLayoutButton = initChildButton(styleToolbarPane, ICON_CHANGE_LAYOUT, TOOLTIP_CHANGE_LAYOUT, CSS_CLASS_BUTTON, false);

        titleBar.getChildren().add(styleToolbarPane);
    }

    public void initWindow(String windowTitle) {
        primaryStage.setTitle(windowTitle);

        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        mainPane = new BorderPane();
        mainPane.setTop(titleBar);
        primaryScene = new Scene(mainPane);

        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    public Button initChildButton(Pane toolbar,
            String iconFileName,
            LanguagePropertyType tooltip,
            String cssClass,
            boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + SLASH + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    public void updateToolbarControls(boolean saved) {
        mainPane.setCenter(workspace);

        saveFileButton.setDisable(saved);
        saveAsFileButton.setDisable(saved);
        viewButton.setDisable(false);
        exportButton.setDisable(false);

        updateItemControlBarTools();
    }

    public void updateItemControlBarTools() {
        addItemButton.setDisable(false);
        boolean itemSelected = collection.isItemSelected();
        removeItemButton.setDisable(!itemSelected);
        moveUpItemButton.setDisable(!itemSelected);
        moveDownItemButton.setDisable(!itemSelected);

        if (collection.getCollection().size() <= 1) {
            moveUpItemButton.setDisable(true);
            moveDownItemButton.setDisable(true);
            rearrangeButton.setDisable(true);
        } else {
            moveUpItemButton.setDisable(false);
            moveDownItemButton.setDisable(false);
            rearrangeButton.setDisable(false);
        }
    }

    public void reloadNavPane() {
        navPane.getChildren().clear();
        for (Item item : collection.getCollection()) {
            Label navItem = new Label(item.getTitle());
            navItem.getStyleClass().add(CSS_CLASS_NAV_PANE_ITEM);
            navItem.setMinHeight(50);
            navItem.setMinWidth(525);
            if (collection.isSelectedItem(item) && collection.getCollection().size() > 0) {
                navItem.getStyleClass().add(CSS_CLASS_SELECTED_ITEM);
            } else {
                navItem.getStyleClass().add(CSS_CLASS_NAV_PANE_ITEM);
            }

            navPane.getChildren().add(navItem);
            navItem.setOnMousePressed(e -> {
                collection.setSelectedItem(item);
                this.reloadNavPane();
            });
        }
        if (collection.isItemSelected()) {
            ItemEditView itemView = new ItemEditView(collection.getSelectedItem());
            updateWorkspace(itemView);
        }
        else {
            updateWorkspace(null);
        }
        updateItemControlBarTools();
    }

    public void setCollection(Collection initCollection) {
        this.collection = initCollection;
    }

    public void updateWorkspace(ItemEditView initView) {
        if (!collection.isItemSelected() || collection.getSelectedItem().equals(null) || collection.getCollection().size() <= 0 || initView == null) {
            workspace.getChildren().clear();
            workspace.getChildren().add(itemControlBar);
            workspace.getChildren().add(navScrollPane);
        } else {
            workspace.getChildren().clear();
            workspace.getChildren().add(itemControlBar);
            workspace.getChildren().add(navScrollPane);
            workspace.getChildren().add(initView);
        }
    }
}

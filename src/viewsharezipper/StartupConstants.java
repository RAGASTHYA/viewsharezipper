/*
 * All contents of this project belongs to Stony Brook University.
 * No content can be copied, shared or reused without the  
 * authorization of the University. 
 */
package viewsharezipper;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * 
 * @author Rahul S Agasthya 
 */
public class StartupConstants {
    
    //Language Constants
    public static String ARABIC_LANG    = "English";
    public static String BENGALI_LANG   = "Bengali";
    public static String CANTONESE_LANG = "Cantonese";
    public static String ENGLISH_LANG   = "English";
    public static String FRENCH_LANG    = "French";
    public static String GERMAN_LANG    = "German";
    public static String HINDI_LANG     = "Hindi";
    public static String ITALIAN_LANG   = "Italian";
    public static String JAPANESE_LANG  = "Japanese";
    public static String KOREAN_LANG    = "Korean";
    public static String MALAY_LANG     = "Malay";
    public static String MANDARIN_LANG  = "Mandarin";
    public static String RUSSIAN_LANG   = "Russian";
    public static String SPANISH_LANG   = "Spanish";
    public static String TAMIL_LANG     = "Tamil";
    public static String URDU_LANG      = "Urdu";
   
    public static String SLASH = "/";
    
    //LANGUAGE XML FILE NAMES
    public static String UI_PROPERTIES_FILE_NAME_Arabic     = "properties_AB.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Bengali    = "properties_BN.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Cantonese  = "properties_CN.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_English    = "properties_EN.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_French     = "properties_FR.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_German     = "properties_GE.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Hindi      = "properties_HD.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Italian    = "properties_IT.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Japanese   = "properties_JP.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Korean     = "properties_KR.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Malay      = "properties_MA.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Mandarin   = "properties_MN.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Russian    = "properties_RU.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Spanish    = "properties_SP.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Tamil      = "properties_TM.xml"; 
    public static String UI_PROPERTIES_FILE_NAME_Urdu       = "properties_UD.xml"; 
    public static String PROPERTIES_SCHEMA_FILE_NAME        = "properties_schema.xsd";
    
    public static String PROPERTY_TYPES_LIST                = "property_types.txt";   
    
    public static String PATH_DATA              = "./Data";
    public static String PATH_COLLECTION_DATA   = PATH_DATA + "/Collections/";
    public static String PATH_XML_FILES         = PATH_DATA;
    public static String PATH_IMAGES            = PATH_DATA + "/Images/";
    public static String PATH_ICONS             = PATH_IMAGES + "Icons/";
    
    public static String PATH_CSS               = "./viewsharezipper/Style";
    public static String STYLE_SHEET_UI         = PATH_CSS + SLASH + "CollectionStyle.css";
    
    //ICONS
    public static String ICON_NEW_COLLECTION        = "new_collection.png";
    public static String ICON_LOAD_COLLECTION       = "load_collection.png";
    public static String ICON_SAVE_COLLECTION       = "save_collection.png";
    public static String ICON_SAVEAS_COLLECTION     = "saveAs_collection.png";
    public static String ICON_VIEW_COLLECTION       = "view_collection.png";
    public static String ICON_EXPORT_COLLECTION     = "export_collection.png";
    public static String ICON_COMPARE_COLLECTION    = "compare_collection.png";
    public static String ICON_REARRANGE_COLLECTION  = "rearrange_collection.png";
    public static String ICON_HELP                  = "help.png";
    public static String ICON_EXIT_COLLECTION       = "exit.png";
    public static String ICON_ADD_ITEM              = "add_item.png";
    public static String ICON_REMOVE_ITEM           = "remove_item.png";
    public static String ICON_MOVE_ITEM_UP          = "move_item_up.png";
    public static String ICON_MOVE_ITEM_DOWN        = "move_item_down.png";
    public static String ICON_CHANGE_FONT           = "change_font.png";
    public static String ICON_CHANGE_COLOR          = "change_color.png";
    public static String ICON_CHANGE_LAYOUT         = "change_layout.png";
    
    //UI SETTINGS
    public static String DEFAULT_IMAGE          = "DefaultImage.png";
    public static String DEAULT_STUDENT_NAME    = "Student Name";
    public static String DEFAUT_STUDENT_ID      = "XXXXXXXXX";
    public static String DEAULT_STUDENT_NETID   = "SName";
    public static String NO_VIDEO_IMAGE         = "NoVideo.png";
    
    public static String DEFAULT_TITLE          = "Title";
    public static String DEFAULT_LOCATION       = "Location";
    public static String DEFAULT_CATEGORY       = "Category";
    public static String DEFAULT_TEXT           = "Description";
    public static String DEFAULT_DATE_ISO       = "Date ISO";
    
    //OTHER DEFAULTS
    public static String DEFAULT_VALUES         = "X";
    public static int    DEFAULT_IMAGE_WIDTH    = 300;
    public static int    DEFAULT_VIDEO_WIDTH    = 300;
        
    //CSS STYLE SHEET CLASSES
    public static String CSS_CLASS_FILE_TOOLBAR_HBOX        = "file_toolbar_HBox";
    public static String CSS_CLASS_STYLE_TOOLBAR_HBOX       = "style_toolbar_HBox";
    public static String CSS_CLASS_ITEM_TOOLBAR_VBOX        = "item_toolbar_VBox";
    public static String CSS_CLASS_NAV_PANE                 = "nav_pane";
    public static String CSS_CLASS_NAV_PANE_ITEM            = "nav_pane_item";
    public static String CSS_CLASS_SELECTED_ITEM            = "selected";
    public static String CSS_CLASS_ITEM_EDIT_VIEW           = "item_edit_view";
    public static String CSS_CLASS_DIALOG_BOX_STYLE         = "dialog_box_style";
    public static String CSS_CLASS_BUTTON                   = "button";
    public static String CSS_CLASS_TITLE_COMPONENT_STYLE    = "title_component";
    public static String CSS_CLASS_COMPONENT_STYLE          = "component";
}

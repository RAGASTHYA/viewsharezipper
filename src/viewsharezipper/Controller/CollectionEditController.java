/*
 * All contents of this project belong to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.Controller;

import properties_manager.PropertiesManager;
import viewsharezipper.Model.Collection;
import static viewsharezipper.StartupConstants.DEFAULT_CATEGORY;
import static viewsharezipper.StartupConstants.DEFAULT_DATE_ISO;
import static viewsharezipper.StartupConstants.DEFAULT_IMAGE;
import static viewsharezipper.StartupConstants.DEFAULT_LOCATION;
import static viewsharezipper.StartupConstants.DEFAULT_TEXT;
import static viewsharezipper.StartupConstants.DEFAULT_TITLE;
import static viewsharezipper.StartupConstants.DEFAULT_VALUES;
import static viewsharezipper.StartupConstants.NO_VIDEO_IMAGE;
import static viewsharezipper.StartupConstants.PATH_IMAGES;
import static viewsharezipper.StartupConstants.SLASH;
import viewsharezipper.View.CollectionView;

/**
 * This controller provides responses for the Collection edit tool bar,
 * which allows the user to add, remove, and reorder items.
 * 
 * @author Rahul S Agasthya
 */
public class CollectionEditController {
    CollectionView ui;
    
    public CollectionEditController(CollectionView initUI) {
        ui = initUI;
    }
    
    public void processAddItemRequest() {
        Collection collection = ui.getCollection();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        collection.addItem(DEFAULT_TITLE + " " + (ui.getCollection().getCollection().size() + 1), DEFAULT_LOCATION, DEFAULT_CATEGORY, PATH_IMAGES + DEFAULT_IMAGE, null, DEFAULT_TEXT, DEFAULT_DATE_ISO);
    }
    
    public void processRemoveItemRequest() {
        Collection collection = ui.getCollection();
        collection.removeSelectedItem();
        ui.reloadNavPane();
    }
    
    public void processMoveItemUpRequest() {
        Collection collection = ui.getCollection();
        collection.moveSelectedItemUp();
        ui.reloadNavPane();
    }
    
    public void processMoveItemDownRequest() {
        Collection collection = ui.getCollection();
        collection.moveSelectedItemDown();
        ui.reloadNavPane();
    }
}

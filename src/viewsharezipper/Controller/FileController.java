/*
 * All contents of this project belong to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import viewsharezipper.File.CollectionFileManager;
import viewsharezipper.Model.Collection;
import static viewsharezipper.StartupConstants.PATH_COLLECTION_DATA;
import viewsharezipper.View.CollectionView;

/**
 *
 * @author Rahul S Agasthya
 */
public class FileController {

    private boolean saved;
    private CollectionView ui;
    private CollectionFileManager collectionIO;

    public FileController(CollectionView initUI, CollectionFileManager initCollectionIO) {
        saved = false;
        ui = initUI;
        collectionIO = initCollectionIO;
    }

    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    private boolean promptToSave() throws FileNotFoundException {
        boolean saveWork = true;

        if (saveWork) {
            Collection collection = ui.getCollection();
            collectionIO.saveCollection(collection);
            saved = true;
        } else if (!true) {
            return true;
        }
        return true;
    }

    private void promptToOpen() {
        FileChooser collectionFileChooser = new FileChooser();
        collectionFileChooser.setInitialDirectory(new File(PATH_COLLECTION_DATA));
        File selectedFile = collectionFileChooser.showOpenDialog(ui.getWindow());

        if (selectedFile != null) {
            try {
                Collection collectionToLOad = ui.getCollection();
                collectionIO.loadCollection(collectionToLOad, selectedFile.getAbsolutePath());
                ui.reloadNavPane();
                ui.updateToolbarControls(saved);
            } catch (IOException ex) {
                Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void handleNewCollectionRequest() {
        boolean continueToMakeNew = true;
        if (!saved) {
            try {
                //continueToMakeNew = promptToSave();
                continueToMakeNew = true;
                
                if (continueToMakeNew) {
                    Collection collection = new Collection(ui);
                    ui.setCollection(collection);
                    collection.reset();
                    saved = false;

                    ui.updateToolbarControls(saved);
                    
                }
            } catch (Exception ex) {
                Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void handleLoadCollectionRequest() {
        boolean continueToOpen = true;
        try {
            if (!saved) {
                continueToOpen = promptToSave();
            }
            if (continueToOpen) {
                promptToOpen();
            }
        } catch (IOException ioe) {

        }
    }

    public boolean handleSaveCollectionRequest() {
        try {
            Collection collectionToSave = ui.getCollection();
            collectionIO.saveCollection(collectionToSave);
            saved = true;
            ui.updateToolbarControls(saved);
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void handleExitRequest() {
        try {
            boolean continueToExit = true;
            if (!saved) {
                continueToExit = promptToSave();
            }
            if (continueToExit) {
                System.exit(0);
            }
        } catch (IOException ioe) {
            
        }
    }
}

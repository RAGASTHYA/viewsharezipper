/*
 * All contents of this project belong to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import viewsharezipper.Model.Collection;
import viewsharezipper.Model.Item;
import static viewsharezipper.StartupConstants.PATH_COLLECTION_DATA;

/**
 *
 * @author Rahul S Agasthya
 */
public class CollectionFileManager {

    public static String JSON_STUDENT_NAME = "student_name";
    public static String JSON_STUDENT_ID = "student_id";
    public static String JSON_STUDENT_NETID = "student_netid";
    public static String JSON_ITEMS = "items";
    public static String JSON_ITEM_HAS_VIDEO = "has_video";
    public static String JSON_ITEM_TITLE = "title";
    public static String JSON_ITEM_LOCATION = "location";
    public static String JSON_ITEM_CATEGORY = "category";
    public static String JSON_ITEM_DATEISO = "date_iso";
    public static String JSON_ITEM_IMAGE_URL = "image_url";
    public static String JSON_ITEM_VIDEO_URL = "video_url";
    public static String JSON_ITEM_TEXT = "text";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    public void saveCollection(Collection collectionToSave) throws FileNotFoundException {
        StringWriter sw = new StringWriter();

        JsonArray itemsJsonArray = makeItemsJsonArray(collectionToSave.getCollection());

        JsonObject collectionJsonObject = Json.createObjectBuilder()
                .add(JSON_STUDENT_NAME, collectionToSave.getStudentName())
                .add(JSON_STUDENT_ID, collectionToSave.getStudentID())
                .add(JSON_STUDENT_NETID, collectionToSave.getStudentNetID())
                .add(JSON_ITEMS, itemsJsonArray)
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(collectionJsonObject);
        jsonWriter.close();

        String title = collectionToSave.getStudentName() + "'s Collection";
        String jsonFilePath = PATH_COLLECTION_DATA + SLASH + title + JSON_EXT;

        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(collectionJsonObject);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(jsonFilePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    public void loadCollection(Collection collectionToLoad, String jsonFilePath) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);

        collectionToLoad.reset();
        collectionToLoad.setStudentName(json.getString(JSON_STUDENT_NAME));
        collectionToLoad.setStudentID(json.getString(JSON_STUDENT_ID));
        collectionToLoad.setStudentNetID(json.getString(JSON_STUDENT_NETID));
        JsonArray jsonItemArray = json.getJsonArray(JSON_ITEMS);
        for (int i = 0; i < jsonItemArray.size(); i++) {
            JsonObject itemJso = jsonItemArray.getJsonObject(i);
            boolean hasVideo = itemJso.getBoolean(JSON_ITEM_HAS_VIDEO);
            if (hasVideo) {
                collectionToLoad.addItem(itemJso.getString(JSON_ITEM_TITLE), itemJso.getString(JSON_ITEM_LOCATION), itemJso.getString(JSON_ITEM_CATEGORY), itemJso.getString(JSON_ITEM_IMAGE_URL), itemJso.getString(JSON_ITEM_VIDEO_URL), itemJso.getString(JSON_ITEM_TEXT), itemJso.getString(JSON_ITEM_DATEISO));
            } else {
                collectionToLoad.addItem(itemJso.getString(JSON_ITEM_TITLE), itemJso.getString(JSON_ITEM_LOCATION), itemJso.getString(JSON_ITEM_CATEGORY), itemJso.getString(JSON_ITEM_IMAGE_URL), null, itemJso.getString(JSON_ITEM_TEXT), itemJso.getString(JSON_ITEM_DATEISO));
            }
        }
    }

    private JsonObject loadJSONFile(String jsonFilePath) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    private JsonArray makeItemsJsonArray(List<Item> items) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Item item : items) {
            JsonObject jso = makeItemJsonObject(item);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makeItemJsonObject(Item item) {
        if (item.containsVideo()) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_ITEM_TITLE, item.getTitle())
                    .add(JSON_ITEM_LOCATION, item.getLocation())
                    .add(JSON_ITEM_CATEGORY, item.getCategory())
                    .add(JSON_ITEM_DATEISO, item.getDateISO())
                    .add(JSON_ITEM_IMAGE_URL, item.getImageURL())
                    .add(JSON_ITEM_VIDEO_URL, item.getVideoURL())
                    .add(JSON_ITEM_TEXT, item.getText())
                    .add(JSON_ITEM_HAS_VIDEO, item.containsVideo())
                    .build();
            return jso;
        }
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_ITEM_TITLE, item.getTitle())
                .add(JSON_ITEM_LOCATION, item.getLocation())
                .add(JSON_ITEM_CATEGORY, item.getCategory())
                .add(JSON_ITEM_DATEISO, item.getDateISO())
                .add(JSON_ITEM_IMAGE_URL, item.getImageURL())
                .add(JSON_ITEM_TEXT, item.getText())
                .add(JSON_ITEM_HAS_VIDEO, item.containsVideo())
                .build();
        return jso;
    }
}

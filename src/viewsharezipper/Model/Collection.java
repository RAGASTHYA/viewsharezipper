/*
 * All contents of this project belongs to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.Model;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import viewsharezipper.LanguagePropertyType;
import static viewsharezipper.LanguagePropertyType.DEFAULT_STUDENT_ID;
import static viewsharezipper.LanguagePropertyType.DEFAULT_STUDENT_NAME;
import static viewsharezipper.LanguagePropertyType.DEFAULT_STUDENT_NETID;
import viewsharezipper.View.CollectionView;

/**
 * This class calls multiple objects of the item class in an Observable List.
 * Details regarding student name, id and netID is stored in this class, with
 * all the items in the Collection.
 *
 * @author Rahul S Agasthya
 */
public class Collection {

    private String studentName;
    private String studentID;
    private String studentNetID;

    ObservableList<Item> collection;
    CollectionView ui;

    Item selectedItem;

    //DEFAULT CONSTRUCTOR
    public Collection(CollectionView initUI) {
        ui = initUI;
        collection = FXCollections.observableArrayList();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        studentName = props.getProperty(DEFAULT_STUDENT_NAME);
        studentID = props.getProperty(DEFAULT_STUDENT_ID);
        studentNetID = props.getProperty(DEFAULT_STUDENT_NETID);
        reset();
    }

    //GETTERS AND SETTERS
    /**
     * This method checks if there is a selected item.
     *
     * @return true if selectedItem != null or false if selectedItem == null
     */
    public boolean isItemSelected() {
        return selectedItem != null;
    }

    /**
     * This method checks if the testItem entered is the selected item.
     *
     * @param testItem is the item to be checked.
     * @return true if selectedItem == testItem or false if selectedItem !=
     * testItem
     */
    public boolean isSelectedItem(Item testItem) {
        return selectedItem == testItem;
    }

    /**
     * This method gets the name of the student.
     *
     * @return studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * This method gets the ID of the student.
     *
     * @return studentID
     */
    public String getStudentID() {
        return studentID;
    }

    /**
     * This method gets the NetID of the student.
     *
     * @return studentNetID
     */
    public String getStudentNetID() {
        return studentNetID;
    }

    /**
     * This method is used to get the list of items.
     *
     * @return collection
     */
    public ObservableList<Item> getCollection() {
        return collection;
    }

    /**
     * This method is used to get the selected item.
     *
     * @return selectedItem
     */
    public Item getSelectedItem() {
        return selectedItem;
    }

    /**
     * This method is used to re-write the name of the student.
     *
     * @param initStudentName studentName is set to the value of
     * initStudentName.
     */
    public void setStudentName(String initStudentName) {
        this.studentName = initStudentName;
    }

    /**
     * This method is used to re-write the ID of the student.
     *
     * @param initStudentID studentID is set to the value of initStudentID.
     */
    public void setStudentID(String initStudentID) {
        this.studentID = initStudentID;
    }

    /**
     * This method is used to re-write the NetID of the student.
     *
     * @param initStudentNetID studentNetID is set to the value of
     * initStudentNetID.
     */
    public void setStudentNetID(String initStudentNetID) {
        this.studentNetID = initStudentNetID;
    }

    /**
     * This method is used to set the value of the selected item.
     *
     * @param initSelectedItem selectedItem is set to the value of
     * initSelectedItem.
     */
    public void setSelectedItem(Item initSelectedItem) {
        this.selectedItem = initSelectedItem;
        //ui.reloadItemEditView()
    }

    //SERVICE METHODS
    /**
     * This method resets the data model of the collection. It clears all items.
     * It sets the values of Student Name, ID and NetID to the default values.
     * It sets selectedItem to null.
     */
    public void reset() {
        collection.clear();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        studentName = props.getProperty(LanguagePropertyType.DEFAULT_STUDENT_NAME);
        studentID = props.getProperty(LanguagePropertyType.DEFAULT_STUDENT_ID);
        studentNetID = props.getProperty(LanguagePropertyType.DEFAULT_STUDENT_NETID);
        selectedItem = null;
    }

    /**
     * This method is used to add one item to the list of items in the
     * Collection.
     *
     * @param initTitle title of the item to be inserted.
     * @param initLocation location of the item to be inserted.
     * @param initCategory category of the item to be inserted.
     * @param initImageURL imageURL of the item to be inserted.
     * @param initVideoURL videoURL, if exists, or null.
     * @param initText test of the item to be inserted.
     * @param initDateISO dateISO of the item to be inserted.
     */
    public void addItem(String initTitle,
            String initLocation,
            String initCategory,
            String initImageURL,
            String initVideoURL,
            String initText,
            String initDateISO) {
        Item itemToAdd;
        if (initVideoURL == null) {
            itemToAdd = new Item(initTitle,
                    initLocation,
                    initCategory,
                    initImageURL,
                    initText,
                    initDateISO);
        } else {
            itemToAdd = new Item(initTitle,
                    initLocation,
                    initCategory,
                    initImageURL,
                    initVideoURL,
                    initText,
                    initDateISO);
        }

        collection.add(itemToAdd);
        this.setSelectedItem(itemToAdd);
        ui.reloadNavPane();
    }

    /**
     * This method removes one, i.e. the selected item from the collection.
     */
    public void removeSelectedItem() {
        if (this.isItemSelected()) {
            Item toRemove = selectedItem;
            int index = collection.indexOf(selectedItem);
            Item toSetSelectedItem;
            if (index == 0 && ((index + 1) < collection.size())) {
                toSetSelectedItem = collection.get(index + 1);
            } else if ((index == collection.size() - 1) && (index - 1) >= 0) {
                toSetSelectedItem = collection.get(index - 1);
            } else if (collection.size() == 1) {
                toSetSelectedItem = null;
            } else {
                toSetSelectedItem = collection.get(index - 1);
            }
            collection.remove(toRemove);
            this.setSelectedItem(toSetSelectedItem);
            ui.reloadNavPane();   
        }
    }

    /**
     * Moves the selected item one step up in the list of items in the
     * collection.
     */
    public void moveSelectedItemUp() {
        if (isItemSelected()) {
            moveItemUp(selectedItem);
            ui.reloadNavPane();
        }
    }

    /**
     * Moves the item passed as the parameter one step up in the list of items
     * of the collection.
     *
     * @param itemToMove The method moves itemToMove one step up.
     */
    private void moveItemUp(Item itemToMove) {
        int index = collection.indexOf(itemToMove);
        if (index > 0) {
            Item temp = collection.get(index);
            collection.set(index, collection.get(index - 1));
            collection.set(index - 1, temp);
        } else if (index == 0) {
            Item temp = collection.remove(index);
            collection.add(temp);
        }
    }

    /**
     * Moves the selected item one step down in the list of items in the
     * collection.
     */
    public void moveSelectedItemDown() {
        if (isItemSelected()) {
            moveItemDown(selectedItem);
            ui.reloadNavPane();
        }
    }

    /**
     * Moves the item passed as the parameter one step down in the list of items
     * of the collection.
     *
     * @param itemToMove The method moves itemToMove one step down.
     */
    private void moveItemDown(Item itemToMove) {
        int index = collection.indexOf(itemToMove);
        if (index < (collection.size() - 1)) {
            Item temp = collection.get(index);
            collection.set(index, collection.get(index + 1));
            collection.set(index + 1, temp);
        } else if (index == (collection.size() - 1)) {
            Item temp = collection.remove(index);
            collection.add(0, temp);
        }
    }
    
    //VALIDATION METHODS
    public boolean validateStudentID() {
        boolean valid = true;
        if(studentID.length() > 9 || !studentID.matches("[0-9]+"))
            valid = false;
        if(studentID == null || studentID.equals(LanguagePropertyType.DEFAULT_STUDENT_ID))
            valid = false;
        return valid;
    }
    
    public boolean validateStudentName() {
        boolean valid = true;
        if(studentName == null || studentName.equals(LanguagePropertyType.DEFAULT_STUDENT_NAME))
            valid = false;
        return valid;
    }
    
    public boolean validateStudentNetID() {
        boolean valid = true;
        if(studentNetID == null || studentNetID.equals(LanguagePropertyType.DEFAULT_STUDENT_NETID))
            valid = false;
        return valid;
    }
    
    public String validateCollection() throws IOException {
        String errors = "ERRORS:\n";
        boolean isValid = true;
        
        if(!validateStudentName()) {
            errors = errors + "Student Name is invalid.\n";
            isValid = false;
        }
        if(!validateStudentID()) {
            errors = errors + "Student ID is invalid.\n";
            isValid = false;
        }
        if(!validateStudentNetID()) {
            errors = errors + "Student NetID is invalid.\n";
            isValid = false;
        }
        String itemErrors = "\nITEM ERRORS: \n";
        boolean allItemsValid = true;
        for(int i=0; i<collection.size(); i++) {
            Item item = collection.get(i);
            String result = item.validateItem();
            if(result.equals("SUCCESS...")) continue;
            else {
                allItemsValid = false;
                isValid = false;
                itemErrors = itemErrors + "\tITEM " + i + " ERRORS:\n";
                itemErrors = itemErrors + result;
            }
        }
        
        if(!allItemsValid) errors = errors + allItemsValid;
        if(isValid) errors = "SUCCESS";
        
        return errors;
    }
}

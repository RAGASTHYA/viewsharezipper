/*
 * All contents of this project belongs to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.Model;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.LatLng;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static viewsharezipper.StartupConstants.DEFAULT_VALUES;

/**
 * This is the data model for each item in the collection. Multiple objects of
 * this class item shall be called in the Collection class as an Observable
 * list.
 *
 * @author Rahul S Agasthya
 */
public class Item {

    private String title;
    private String location;
    private String category;
    private String imageURL;
    private String videoURL;
    private String text;
    private String dateISO;
    
    private boolean containsVideo;

    //DEFAULT CONSTRUCTOR
    public Item() {
        title = DEFAULT_VALUES;
        location = DEFAULT_VALUES;
        category = DEFAULT_VALUES;
        imageURL = DEFAULT_VALUES;
        videoURL = DEFAULT_VALUES;
        text = DEFAULT_VALUES;
        dateISO = DEFAULT_VALUES;
        containsVideo = false;
    }

    //CONSTRUCTOR WITH VIDEO_URL
    public Item(String initTitle, String initLocation, String initCategory, String initImageURL, String initVideoURL, String initText, String initDateISO) {
        this.title = initTitle;
        this.location = initLocation;
        this.category = initCategory;
        this.imageURL = initImageURL;
        this.videoURL = initVideoURL;
        this.text = initText;
        this.dateISO = initDateISO;
        containsVideo = true;
    }

    //CONSTRUCTOR WITHOUT VIDEO_URL
    public Item(String initTitle, String initLocation, String initCategory, String initImageURL, String initText, String initDateISO) {
        this.title = initTitle;
        this.location = initLocation;
        this.category = initCategory;
        this.imageURL = initImageURL;
        this.videoURL = DEFAULT_VALUES;
        this.text = initText;
        this.dateISO = initDateISO;
        containsVideo = false;
    }

    //GETTERS AND SETTERS
    /**
     * This method is used to get the title of the item object.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method is used to re-write the title of the item object.
     *
     * @param initTitle The title of the item object is set to the value stored
     * in initTitle.
     */
    public void setTitle(String initTitle) {
        this.title = initTitle;
    }

    /**
     * This method is used to get the location of the item object.
     *
     * @return location
     */
    public String getLocation() {
        return location;
    }

    /**
     * This method is used to re-write the location of the item object.
     *
     * @param initLocation location is set to the value stored in initLocation.
     */
    public void setLocation(String initLocation) {
        this.location = initLocation;
    }

    /**
     * This method is used to get the category of the item object.
     *
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * This method is used to re-write the category of the item object.
     *
     * @param initCategory category is set to the value stored in initCategory.
     */
    public void setCategory(String initCategory) {
        this.category = initCategory;
    }

    /**
     * This method is used to get the imageURL of the item object.
     *
     * @return imageURL
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * This method is used to re-write the imageURL of the item object.
     *
     * @param initImageURL imageURL is set to the value stored in initImageURL.
     */
    public void setImageURL(String initImageURL) {
        this.imageURL = initImageURL;
    }

    /**
     * This method is used to get the videoURL of the item object.
     *
     * @return videoURL
     */
    public String getVideoURL() {
        return videoURL;
    }

    /**
     * This method is used to re-write the videoURL of the item object.
     *
     * @param initVideoURL videoURL is set to the value stored in initVideoURL.
     */
    public void setVideoURL(String initVideoURL) {
        if(initVideoURL != null) containsVideo = true;
        else containsVideo = false;
        this.videoURL = initVideoURL;
    }

    /**
     * This method is used to get the text of the item object.
     *
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * This method is used to re-write the text of the item object.
     *
     * @param initText text is set to the value stored in initText.
     */
    public void setText(String initText) {
        this.text = initText;
    }

    /**
     * This method is used to get the dateISO of the item object.
     *
     * @return dateISO
     */
    public String getDateISO() {
        return dateISO;
    }

    /**
     * This method is used to re-write the dateISO of the item object.
     *
     * @param initDateISO dateISO is set to the value stored in initDateISO.
     */
    public void setDateISO(String initDateISO) {
        this.dateISO = initDateISO;
    }

    //VALIDATION METHODS
    /**
     * This method is used to validate the location of the item.
     *
     * @return true if location is valid, else false.
     * @throws IOException if location is invalid.
     */
    public boolean validateLocation() throws IOException {
        LatLng geoLocation = new LatLng();
        if (location == null) {
            return false;
        }
        try {
            Geocoder geocoder = new Geocoder();
            GeocoderRequest geocoderRequest;
            GeocodeResponse geocodeResponse;
            String addr;
            BigDecimal lat;
            BigDecimal lng;
            geocoderRequest = new GeocoderRequestBuilder().setAddress(location).getGeocoderRequest();
            geocodeResponse = geocoder.geocode(geocoderRequest);
            addr = geocodeResponse.getResults().get(0).getFormattedAddress();
            lat = BigDecimal.valueOf(geocodeResponse.getResults().get(0).getGeometry().getLocation().getLat().doubleValue());
            lng = BigDecimal.valueOf(geocodeResponse.getResults().get(0).getGeometry().getLocation().getLng().doubleValue());
            geoLocation = new LatLng(lat, lng);
        } catch (Exception e) {
            geoLocation = new LatLng(BigDecimal.valueOf(91), BigDecimal.valueOf(181));
            return false;
        }
        return true;
    }

    /**
     * This method is used to validate the dateISO of the item.
     *
     * @return true if valid, else false.
     */
    public boolean validateDateISO() {
        if (dateISO == null) {
            return false;
        }
        String date = dateISO;
        if (dateISO.charAt(0) == '-') {
            date = dateISO.substring(1);
        }
        String[] components = date.split("-");
        boolean valid = true;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        sdf.setLenient(false);
        try {
            Date dateParsed = sdf.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * This method validates the title of the item.
     *
     * @return true if valid, else false.
     */
    public boolean validateTitle() {
        return title != null || !title.equals("X");
    }

    /**
     * This method validates the imageURL of the item.
     *
     * @return true if valid, else false.
     */
    public boolean validateImageURL() {
        if (imageURL == null || imageURL.equals("X")) {
            return false;
        }
        return validateURL(imageURL);
    }

    /**
     * This method validates the videoURL of the item.
     *
     * @return true if valid, else false.
     */
    public boolean validateVideoURL() {
        if (this.containsVideo()) {
            if (videoURL == null || videoURL.equals("X")) {
                return false;
            }
            return validateURL(videoURL);
        } else {
            if (videoURL == "X") {
                return true;
            }
            else return false;
        }

    }

    /**
     * This method is used to validate any web based url. This methoed is called
     * in the validateImageURL() and validateVideoURL().
     *
     * @param URL The URL to be validated.
     * @return true if valid, else false.
     */
    public boolean validateURL(String URL) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(URL).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * This method is used to validate the text of the item.
     *
     * @return true if valid, else false.
     */
    public boolean validateText() {
        return text != null || !text.equals("X");
    }

    /**
     * This method is used to validate the category of the item.
     *
     * @return true if valid, else false.
     */
    public boolean validateCategory() {
        return category != null || !category.equals("X");
    }

    /**
     * Checks if video is present in the item or not.
     *
     * @return true if video is present, else false.
     */
    public boolean containsVideo() {
        return containsVideo;
    }

    public String validateItem() throws IOException {
        boolean isItemValid = true;
        String itemErrors = "";
        if (!validateTitle()) {
            itemErrors = itemErrors + "\t\tTitle is invalid.\n";
            isItemValid = false;
        }
        if (!validateLocation()) {
            itemErrors = itemErrors + "\t\tLocation is invalid.\n";
            isItemValid = false;
        }
        if (!validateCategory()) {
            itemErrors = itemErrors + "\t\tCategory is invalid.\n";
            isItemValid = false;
        }
        if (!validateDateISO()) {
            itemErrors = itemErrors + "\t\tDateISO is invalid.\n";
            isItemValid = false;
        }
        if (!validateImageURL()) {
            itemErrors = itemErrors + "\t\tImageURL is invalid.\n";
            isItemValid = false;
        }
        if (!validateVideoURL()) {
            itemErrors = itemErrors + "\t\tVideoURL is invalid.\n";
            isItemValid = false;
        }
        if (!validateText()) {
            itemErrors = itemErrors + "\t\tText is invalid.\n";
            isItemValid = false;
        }
        
        if(isItemValid) return "SUCCESS...";
        return itemErrors;
    }

    /**
     * @todo A method may be required to write the items to JavaScript or HTML.
     * We shall implement that later, which shall be similar to toString()
     * method.
     *
     * public String toJScript() {}
     */
}

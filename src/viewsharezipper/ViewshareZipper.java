/*
 * All contents of this project belongs to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import viewsharezipper.File.CollectionFileManager;
import static viewsharezipper.LanguagePropertyType.TITLE_WINDOW;
import static viewsharezipper.StartupConstants.PATH_DATA;
import static viewsharezipper.StartupConstants.PATH_XML_FILES;
import static viewsharezipper.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static viewsharezipper.StartupConstants.UI_PROPERTIES_FILE_NAME_English;
import viewsharezipper.View.CollectionView;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Rahul S Agasthya
 */
public class ViewshareZipper extends Application{
    
    CollectionFileManager fileManager = new CollectionFileManager();

    CollectionView ui = new CollectionView(fileManager);

    public void start(Stage primaryStage) {
        String langauge = "EN";
        boolean success = loadProperties(langauge);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String appTitle = props.getProperty(TITLE_WINDOW);
        ui.startUI(primaryStage, appTitle);
    }

    public boolean loadProperties(String languageCode) {
        try {
            // FIGURE OUT THE PROPER FILE NAME
            String propertiesFileName = "properties_EN.xml";

            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(UI_PROPERTIES_FILE_NAME_English, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            return false;
        }
    }
    
    public static void main(String[] args) {
	launch(args);
    }
}


/*
 * All contents of this project belong to Stony Brook University.
 * No content can be copied, shared or reused without the 
 * authorization of the University.
 */
package viewsharezipper.Error;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import viewsharezipper.LanguagePropertyType;
import viewsharezipper.View.CollectionView;

/**
 * This class provides error messages to the user when they occur. Note that
 * error messages should be retrieved from language-dependent XML Files and 
 * should have custom messages that are different depending on the type of error
 * so as to be informative concerning what went wrong.
 * 
 * @author Rahul S Agasthya
 */
public class ErrorHandler {
    //APPLICATION UI
    private CollectionView ui;
    
    //KEEP THE APPLICATION UI FOR LATER USE
    public ErrorHandler(CollectionView initUI) {
        ui = initUI;
    }
    
    public void processError(LanguagePropertyType errorType) {
        //GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String errorFeedbackText = props.getProperty(errorType);
        
        //POP OPEN A DIALOG TO DISPLAY TO THE USER
        Stage errorAlert = new Stage();
        Text error = new Text();
        
        error.setText(errorFeedbackText);
        BorderPane pane = new BorderPane();
        Button OK = new Button("OK");
        
        error.setTextAlignment(TextAlignment.CENTER);
        pane.setCenter(error);
        OK.setAlignment(Pos.CENTER);
        pane.setBottom(OK);
        errorAlert.setScene(new Scene(pane));
        errorAlert.show();
        OK.setOnAction( e-> {
            errorAlert.close();
        });
        
    }
}
